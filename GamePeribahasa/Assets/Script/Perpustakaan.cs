﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perpustakaan : MonoBehaviour 
{
		public void GoToPerpustakaan()
	{
		Application.LoadLevel("Perpustakaan");
	}

	public void GoToMenuUtama()
	{
		Application.LoadLevel("MenuUtama");
	}

	public void GoToMenuUtama2()
	{
		Application.LoadLevel("MenuUtama2");
	}

	public void GoToCaraBermain()
	{
		Application.LoadLevel("CaraBermain");
	}

	public void GoToTentang()
	{
		Application.LoadLevel("Tentang");
	}

	public void GoToDesa()
	{
		Application.LoadLevel("Village");
	}

	public void GoToStageNafian()
	{
		Application.LoadLevel("StageBerakit");
	}

	public void GoToDesa2()
	{
		Application.LoadLevel("Village2");
	}

	public void GoToMisi2()
	{
		Application.LoadLevel("StageDua");
	}

	public void GoToMisi3()
	{
		Application.LoadLevel("StageTiga");
	}

	public void GoToDesa3()
	{
		Application.LoadLevel("Village3");
	}

	public void GoToDesa5()
	{
		Application.LoadLevel("Village5");
	}

	public void keluar()
	{
		Application.Quit ();
	}
}