﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour {

	public int loadSceneName; //Next load scene name
	public Texture2D buttonNormal,buttonDown; //Texture button down,normal
	public AudioClip buttonSfx;//button sfx


	void OnMouseUp()
	{
		this.GetComponent<GUITexture>().texture = buttonNormal;
		Invoke("LoadAsynchronously",0.1f);
	}

	void OnMouseDown()
	{
		if(buttonSfx != null)
			AudioSource.PlayClipAtPoint(buttonSfx,transform.position);

		this.GetComponent<GUITexture>().texture = buttonDown;
	}



	IEnumerator LoadAsynchronously (int loadSceneName)
	{
		AsyncOperation operation = SceneManager.LoadSceneAsync (loadSceneName);

		while (!operation.isDone) {
			Debug.Log (operation.progress);
			yield return null;
		}
	}

	public void LoadScene (int loadSceneName)
	{
		StartCoroutine (LoadAsynchronously (loadSceneName));
	}
}
