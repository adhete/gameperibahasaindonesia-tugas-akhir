﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pickup1 : MonoBehaviour {

	public Text countText;
	//public Animator ClearMisi;
	public GameObject btnClear;
	//public Text winText;

	private Rigidbody rb;
	private int count;

	void Start ()
	{
		rb = GetComponent<Rigidbody>();
		count = 0;
		SetCountText ();
		//ClearMisi.SetBool ("IsOpen", false);
		//winText.text = "";
	}
		
	void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.CompareTag ( "Pick Up"))
		{
			other.gameObject.SetActive (false);
			count = count + 1;
			SetCountText ();
		}
	}

	void SetCountText ()
	{
		countText.text = "Jamur: <" + count.ToString () + "/5>";
		if (count >= 5)
		{
			//winText.text = "You Win!";
			//ClearMisi.SetBool ("IsOpen", true);
			btnClear.SetActive(true);
			//Application.LoadLevel("Village");
		}
	}
}