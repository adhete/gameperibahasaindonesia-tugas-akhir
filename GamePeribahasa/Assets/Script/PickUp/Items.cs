﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Items : MonoBehaviour {

	public int item;
	GameObject itemUI;

	void Start()
	{
		itemUI = GameObject.Find("Items");
	}
	void Update()
	{
		itemUI.GetComponent<Text>().text = item.ToString();
		if (item < 0)
		{
			item = 0;
		}
	}
}
