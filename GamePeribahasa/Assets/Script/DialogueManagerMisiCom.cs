﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManagerMisiCom : MonoBehaviour {

	public Text nameText;
	public Text dialogueText;


	public Animator animator;

	public GameObject misiSelesai; //-----

	private Queue<string> sentences;
	// Use this for initialization
	void Start () {
		sentences = new Queue<string> ();
		misiSelesai.SetActive (false);
	}

	public void StartDialogue (Dialogue dialogue)
	{
		animator.SetBool ("IsOpen", true);

		nameText.text = dialogue.nama;

		sentences.Clear();

		foreach (string sentence in dialogue.sentences) 
		{
			sentences.Enqueue(sentence);
		}

		DisplayNextSentence();
	}

	public void DisplayNextSentence ()
	{
		if (sentences.Count == 0) 
		{
			EndDialogue();
			return;
		}

		string sentence = sentences.Dequeue();
		StopAllCoroutines ();
		StartCoroutine (TypeSentence (sentence));
	}

	IEnumerator TypeSentence (string sentence)
	{
		dialogueText.text = "";
		foreach (char letter in sentence.ToCharArray())
		{
			dialogueText.text += letter;
			yield return null;
		}
	}

	public void EndDialogue()
	{
		animator.SetBool ("IsOpen", false);
		misiSelesai.SetActive (true);
	}
}
