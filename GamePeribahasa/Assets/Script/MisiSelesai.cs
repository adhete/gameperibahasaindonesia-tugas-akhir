﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MisiSelesai : MonoBehaviour 
{

	public GameObject guiMisiSelesai;

	// Use this for initialization
	void Start ()
	{
		guiMisiSelesai.SetActive(true);
	}

	// Update is called once per frame 
	void OnTriggerStay (Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			guiMisiSelesai.SetActive(true);

		}

	}
	void OnTriggerExit()
	{
		guiMisiSelesai.SetActive(false);
	}

}
