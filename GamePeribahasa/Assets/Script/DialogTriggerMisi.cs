﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTriggerMisi : MonoBehaviour {
	public Dialogue dialogue;
	public void TriggerDialogueMisiSelesai()
	{
		FindObjectOfType<DialogueManagerMisiCom>().StartDialogue (dialogue);
	}
}
