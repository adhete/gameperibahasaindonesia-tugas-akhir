﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextPertanyaan : MonoBehaviour {

	//public static bool GameIsPaused = false;

	//public GameObject PauseMenuUI;
	public GameObject PauseButton;
	public GameObject PausePanel;
	public GameObject NextSoal;
	public GameObject ScoreAwal;
	public GameObject ScoreAkhir;
	public GameObject UjianSelesai;
	public GameObject UjianGagal;

	void Start()
	{
		PausePanel.SetActive (false);
		NextSoal.SetActive (false);
		ScoreAwal.SetActive (false);
		ScoreAkhir.SetActive (false);
		UjianSelesai.SetActive (false);
		UjianGagal.SetActive (false);
	}

	// Update is called once per frame
	void Update () 
	{

	}


	public void Pause()
	{
		//PauseMenuUI.SetActive (true);
		//Time.timeScale = 0f;
		//GameIsPaused = true;
		Time.timeScale=0;
		PauseButton.SetActive (false);
		PausePanel.SetActive(true);

	}

	public void Resume()
	{
		//PauseMenuUI.SetActive (false);
		//Time.timeScale = 1f;
		//GameIsPaused = false;
		Time.timeScale=1;
		PauseButton.SetActive (false);
		//PausePanel.SetActive(false);
		NextSoal.SetActive (true);
		ScoreAwal.SetActive (false);
		ScoreAkhir.SetActive (true);
	}

	public void JwbSalah()
	{
		//PauseMenuUI.SetActive (false);
		//Time.timeScale = 1f;
		//GameIsPaused = false;
		Time.timeScale=1;
		PauseButton.SetActive (false);
		//PausePanel.SetActive(false);
		NextSoal.SetActive (true);
		ScoreAwal.SetActive (true);
		ScoreAkhir.SetActive (false);
	}

	public void Selesai()
	{
		//PauseMenuUI.SetActive (false);
		//Time.timeScale = 1f;
		//GameIsPaused = false;
		Time.timeScale=1;
		PauseButton.SetActive (false);
		PausePanel.SetActive(false);
		NextSoal.SetActive (false);
		ScoreAwal.SetActive (false);
		ScoreAkhir.SetActive (false);
		UjianSelesai.SetActive (true);
	}

	public void BerhasilUjian()
	{
		//PauseMenuUI.SetActive (false);
		//Time.timeScale = 1f;
		//GameIsPaused = false;
		Time.timeScale=1;
		PauseButton.SetActive (false);
		PausePanel.SetActive(false);
		NextSoal.SetActive (false);
		ScoreAwal.SetActive (false);
		ScoreAkhir.SetActive (false);
		UjianSelesai.SetActive (false);
		UjianGagal.SetActive (false);
	}

	public void Gagal()
	{
		//PauseMenuUI.SetActive (false);
		//Time.timeScale = 1f;
		//GameIsPaused = false;
		Time.timeScale=1;
		PauseButton.SetActive (false);
		PausePanel.SetActive(false);
		NextSoal.SetActive (false);
		ScoreAwal.SetActive (false);
		ScoreAkhir.SetActive (false);
		UjianGagal.SetActive (true);
	}
}
