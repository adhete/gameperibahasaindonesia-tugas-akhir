﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ItemSelection : MonoBehaviour {

	public Image SelectionImage;
	public List<Sprite> ItemList = new List<Sprite>();
	private int itemSpot=0;
	public GameObject panel;
	public void RightSelection()
	{
		if (itemSpot < ItemList.Count - 1) 
		{
			itemSpot++;
			SelectionImage.sprite = ItemList [itemSpot];
		}
	}

	public void LeftSelection()
	{
		if (itemSpot > 0) 
		{
			itemSpot--;
			SelectionImage.sprite = ItemList [itemSpot];
		}
	}

	public void Resume()
	{
		//PauseMenuUI.SetActive (false);
		//Time.timeScale = 1f;
		//GameIsPaused = false;
		Time.timeScale=1;
		panel.SetActive (false);
	}

	// Use this for initialization
	void Start () {
		//panel.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
