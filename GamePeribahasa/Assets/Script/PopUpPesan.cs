﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpPesan : MonoBehaviour {

	public GameObject guiObject;

	void OnTriggerStay (Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			guiObject.SetActive(true);

		}

	}
	void OnTriggerExit()
	{
		guiObject.SetActive(false);
	}
		
}
