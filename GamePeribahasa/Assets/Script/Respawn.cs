﻿﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Respawn : MonoBehaviour {

	private Vector3 startPos;
	private Quaternion startRot;
	// Use this for initialization
	void Start () 
	{
		startPos = transform.position;
		startRot = transform.rotation;	
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.tag == "Death")
		{
			transform.position = startPos;
			transform.rotation = startRot;
		}

	}
}
