﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collisions : MonoBehaviour {

	void OnTriggerEnter(Collider other) {
		Debug.Log (name + " entered triger with " + other.name);
	}

	void OnCollisionEnter(Collision other) {
		Debug.Log (name + " entered collision with " + other.gameObject.name);
	}
}
