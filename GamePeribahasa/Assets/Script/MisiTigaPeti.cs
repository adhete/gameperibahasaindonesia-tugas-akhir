﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MisiTigaPeti : MonoBehaviour {

	//public static bool GameIsPaused = false;

	//public GameObject PauseMenuUI;
	public GameObject PauseButton;
	public GameObject PausePanel;
	public GameObject PopMisiSelesai;

	void Start()
	{
		PausePanel.SetActive (false);
		PopMisiSelesai.SetActive (false);
	}

	// Update is called once per frame
	void Update () 
	{

	}

	public void PauseGame()
	{
		//if (GameIsPaused) {
		//Resume ();
		//} else {
		//	Pause ();
		//}
	}
	public void Pause()
	{
		//PauseMenuUI.SetActive (true);
		//Time.timeScale = 0f;
		//GameIsPaused = true;
		Time.timeScale=0;
		PauseButton.SetActive (false);
		PausePanel.SetActive(true);
	}

	public void Resume()
	{
		//PauseMenuUI.SetActive (false);
		//Time.timeScale = 1f;
		//GameIsPaused = false;
		Time.timeScale=1;
		PauseButton.SetActive (true);
		PausePanel.SetActive(false);

	}

	public void ResumeMisiSelesai()
	{
		//PauseMenuUI.SetActive (false);
		//Time.timeScale = 1f;
		//GameIsPaused = false;
		Time.timeScale=1;
		PauseButton.SetActive (true);
		PausePanel.SetActive(false);
		PopMisiSelesai.SetActive (true);
	}

	public void GoToMenuUtama()
	{
		SceneManager.LoadScene ("MenuUtama");
	}

	public void Kembali()
	{
		SceneManager.LoadScene ("Village5");
	}
}
