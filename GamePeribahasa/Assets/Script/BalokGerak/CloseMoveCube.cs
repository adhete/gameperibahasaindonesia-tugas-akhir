﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseMoveCube : MonoBehaviour {

	public GameObject Player;

	void OnTriggerEnter()
	{
		Player.transform.parent = null;
	}
}
