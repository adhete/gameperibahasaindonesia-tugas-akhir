﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class KeluarButton : MonoBehaviour {

	public Texture2D buttonNormal,buttonDown; //Texture button down,normal
	public AudioClip buttonSfx;//button sfx

	//public void ExitGame()
	//{
	//	Application.Quit ();
	//}

	void OnMouseUp()
	{
		this.GetComponent<GUITexture>().texture = buttonNormal;
		Application.Quit ();
		//Invoke("LoadScene",0.1f);
	}

	void OnMouseDown()
	{
		if(buttonSfx != null)
			AudioSource.PlayClipAtPoint(buttonSfx,transform.position);

		this.GetComponent<GUITexture>().texture = buttonDown;
	}

	public void keluar()
	{
		//this.GetComponent<GUITexture>().texture = buttonNormal;
		Application.Quit ();
		//Invoke("LoadScene",0.1f);
	}
}
