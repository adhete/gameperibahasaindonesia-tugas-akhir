﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Petunjuk : MonoBehaviour {

	//public static bool GameIsPaused = false;

	//public GameObject PauseMenuUI;
	//public GameObject ResumeButton;
	public GameObject PetunjukPanel;

	void Start()
	{
		PetunjukPanel.SetActive (true);
	}
		
	public void PanelPetunjuk()
	{
		Time.timeScale=0;
		//ResumeButton.SetActive (true);
		PetunjukPanel.SetActive(true);
	}

	public void Resume()
	{
		Time.timeScale=1;
		//ResumeButton.SetActive (false);
		PetunjukPanel.SetActive(false);
	}
		
}
